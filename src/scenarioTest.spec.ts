import AccountService from "./services/accountService"

jest.useFakeTimers()

beforeAll(()=>{
})
afterEach(() => {
    jest.resetAllMocks();
});

describe('Scenario test', () => {
    const app = AccountService.createInstance();

    it('should be same instance singleton pattern', () => {
        const app1 = AccountService.createInstance();
        const app2 = AccountService.createInstance();
        expect(app1).toEqual(app2)
    })

    it('should be Register two account', () => {
        app.setUsername("bob").setPin(1234).register()
        app.setUsername("alice").setPin(1234).register()
        expect(app.getLists().length).toEqual(2)
    })

    it('should get balance alice 0', () => {
        const alice = app.setUsername("alice").setPin(1234).myAccount()
        expect(alice.balance).toEqual(0)
    })

    it('should deposit and get balance alice 100', () => {
        const alice = app.myAccount()
        app.setBalance(100).deposit()
        expect(alice.balance).toEqual(100)
    })

    it('should get balance bob 0', () => {
        const bob = app.setUsername("bob").setPin(1234).myAccount()
        expect(bob.balance).toEqual(0)
    })

    it('should deposit and get balance bob 80', () => {
        const bob = app.myAccount()
        app.setBalance(80).deposit()
        expect(bob.balance).toEqual(80)
    })

    it('should transfer alice 50 and get balance bob 30', () => {
        app.setTransfer({
            username: "alice",
            total: 50
        })
        .transfer()
        const bob =  app.myAccount()
        expect(bob.balance).toEqual(30)
    })

    it('should transfer alice 100, get balance bob 0 and owed to alice 70', () => {
        const transfer_to_alice = app.setTransfer({
            username: "alice",
            total: 100
        }).transfer()
        const bob_owed_to_alice = app.getMyDebt()[0].total
        const bob =  app.myAccount()

        expect(transfer_to_alice).toEqual(0)
        expect(bob_owed_to_alice).toEqual(70)
        expect(bob.balance).toEqual(0)
    })

    it('should transfer alice 30, get balance bob 0 and owed to alice 40', () => {
        const deposit_and_transfer = app.setBalance(30).deposit()[0].total
        const bob_owed_to_alice = app.getMyDebt()[0].total
        const bob =  app.myAccount()

        expect(deposit_and_transfer).toEqual(30)
        expect(bob_owed_to_alice).toEqual(40)
        expect(bob.balance).toEqual(0)
    })

    it('should get balance alice 210 and get owed from bob 30', () => {
        const alice = app.setUsername("alice").setPin(1234).myAccount()
        const alice_owed_from_bob = app.getMyClaim()[0].total

        expect(alice.balance).toEqual(210)
        expect(alice_owed_from_bob).toEqual(40)
    })

    it('should get balance alice 210, transfered 0 to bob and get owed from bob 10', () => {
        const transfer_to_bob = app.setTransfer({
            username: "bob",
            total: 30
        }).transfer()
        const alice = app.myAccount()
        const alice_owed_from_bob = app.getMyClaim()[0].total

        expect(transfer_to_bob).toEqual(0)
        expect(alice.balance).toEqual(210)
        expect(alice_owed_from_bob).toEqual(10)
    })

    it('should get balance bob 0 and get owed to alice 10', () => {
        const bob = app.setUsername("bob").setPin(1234).myAccount()
        const bob_owed_to_alice = app.getMyDebt()[0].total

        expect(bob.balance).toEqual(0)
        expect(bob_owed_to_alice).toEqual(10)
    })

    it('should deposit 100, transfered to alice 10, get balance bob 90 and get owed to alice 0 or false', () => {
        const deposit_and_transfer = app.setBalance(100).deposit()[0].total
        const bob_owed_to_alice = app.getMyDebt()
        const bob = app.myAccount()

        expect(deposit_and_transfer).toEqual(10)
        expect(bob_owed_to_alice).toEqual(false)
        expect(bob.balance).toEqual(90)
    })

})