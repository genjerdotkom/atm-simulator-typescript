import chalk from "chalk";
class ScreenInfo{

    clear(): void {
        process.stdout.write('\u001B[2J\u001B[0;0f');
    }
 
    welcome(): any {
        return [
            chalk`{cyan ----------------------}`,
            chalk`{cyan \nWelcome Atm Simulator. \nplease choose options}`,
            chalk`{cyan \n----------------------}\n`
        ]
    }

    login(): any {
        return [
            chalk`{cyan \nPlease login.\n}`
        ]
    }

    deposit(): any {
        return [
            chalk`{whiteBright \nDeposit\n----------}{cyan \nEnter amount.\n}`
        ]
    }

    transfer(): any {
        return [
            chalk`{whiteBright \nTransfer\n----------}{cyan \nEnter username target and amount.\n}`
        ]
    }

    withdraw(): any {
        return [
            chalk`{whiteBright \nWithdraw\n----------}{cyan \nEnter amount.\n}`
        ]
    }

    logout(username: string): any {
        return [
            chalk`{greenBright \nGoodbye, ${username}!}\n` 
        ]
    }

    register(): any {
        return [
            chalk`{cyan \nCreate new account by entering username and pin.\n}`
        ]
    }

    balance(username: string, balance: number): any {
        return [
            chalk`{cyan ----------------------}`,
            chalk`{cyan \nhi,} {yellow ${username}}\n{cyan Your balance is} {yellow $${balance}}`,
            chalk`{cyan \n----------------------}`,
        ] 
    }

    usernameExist(): any {
        return [
            chalk`{redBright \nUsername exists}\n`,
        ] 
    }

    usernameNotExist(): any {
        return [
            chalk`{redBright \nUsername not exists}\n`,
        ] 
    }

    wrongCredential(): any {
        return [
            chalk`{redBright \nWrong credentials. please create one if you dont have account.\n}`
        ]  
    }

    wrongPin(): any {
        return [
            chalk`{redBright \nWrong pin!\n}`
        ]  
    }

    success(): any {
        return [
            chalk`{greenBright \nSuccess!\n}`
        ]
    }

    failedSelfTransfer(): any {
        return [
            chalk`{redBright \nYou can't transfer to your own account!\n}`
        ]
    }

    failedWithDraw(): any {
        return [
            chalk`{redBright \nYour balance not enough!\n}`
        ]
    }

    successWithDraw(amount: number): any {
        return [
            chalk`{greenBright \nSuccess withdraw {yellow $${amount}}!\n}`
        ]
    }

    successTransfer(username: string, amount: number): any {
        return [
            chalk`{greenBright \nTransfered {yellow $${amount}} to ${username}!\n}`
        ]
    }

    myDebt(data: any): any {
        let array: any = []
        if(data.length > 0) {
            data.map((item: any) => {
                array.push(chalk`{magenta ----------------------\nOwed {yellow $${item.total}} to ${item.username}!\n}`)
            })
        }
        return array
    }

    myClaim(data: any): any {
        let array: any = []
        if(data.length > 0) {
            data.map((item: any) => {
                array.push(chalk`{greenBright ----------------------\nOwed {yellow $${item.total}} from ${item.username}.\n}`)
            })
        }
        return array
    }

    payMyDebt(data: any): any {
        let array: any = []
        if(data.length > 0) {
            data.map((item: any) => {
                array.push(chalk`{greenBright ----------------------\nTransfered {yellow $${item.total}} to ${item.username}.\n}`)
            })
        }
        return array
    }

    custom(str: string): void {
        console.log(str)  
    }

}

export default new ScreenInfo;