type TransferType = {
    username: string;
    total: number
}

type TempType = { 
    username: string,
    transfer: TransferType,
    withdraw: number,
    pin: number,
    balance: number,
    debt: TransferType
};

type listsType = Array<{
    username: string,
    balance: number,
    pin: number,
    debt: Array<TransferType>
}> | any

export {
    TransferType,
    TempType,
    listsType
}