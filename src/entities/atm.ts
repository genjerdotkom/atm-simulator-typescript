enum StatusAtm {
    LISTENING = 'LISTENING',
    IN_SESSION = 'IN_SESSION',
    END_SESSION = 'END_SESSION'
}

type SessionType = {
    username?: string | any
}

export {
    StatusAtm,
    SessionType
}