import prompt from "prompt";
import AccountService from "./accountService";
import PromptService from "./promptService";
import screenInfo from "../helpers/screenInfo";
import { 
    StatusAtm,
    SessionType 
} from "../entities/atm";
import { 
    LoginSchema,
    RegisterSchema,
    MenuDefaultSchema,
    MenuTransaction,
    DepositSchema,
    TransferSchema,
    WithdrawSchema
} from "../schemas/promptSchema";
class AtmService {
    protected session: SessionType;
    protected status?: StatusAtm;
    private promptService: PromptService
    private account: AccountService
    private static instance:AtmService;

    constructor(){
        prompt.start();
        this.promptService = PromptService.createInstance();
        this.account = AccountService.createInstance();
        this.session = {
            username: ''
        };
    }

    public static createInstance(): AtmService{
        if(!AtmService.instance){
            AtmService.instance = new AtmService();
        }
        return AtmService.instance;
    } 

    on(): void {
        this.status = StatusAtm.LISTENING
        this.promptService.get( 
            MenuDefaultSchema, 
            this.menuDefault.bind(this), 
            [ screenInfo.welcome().join("") ] 
        )
    }

    menuDefault(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        switch (parseInt(cb.menu)){
            case 1:
                this.promptService.get( 
                    LoginSchema, 
                    this.login.bind(this), 
                    [ screenInfo.login().join("") ] 
                )
                break;
            case 2:
                this.promptService.get( 
                    RegisterSchema, 
                    this.register.bind(this), 
                    [ screenInfo.register().join("") ] 
                )
                break;
            default:
                this.promptService.get(
                    MenuDefaultSchema, 
                    this.menuDefault.bind(this)
                )
                break;
        }   
    }

    menuTransaction(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        switch (parseInt(cb.menu)){
            case 1:
                this.promptService.get( 
                    DepositSchema, 
                    this.deposit.bind(this),
                    [ screenInfo.deposit().join("") ]
                )
                break;
            case 2:
                this.promptService.get( 
                    TransferSchema, 
                    this.transfer.bind(this),
                    [ screenInfo.transfer().join("") ]           
                )
                break;
            case 3:
                this.promptService.get( 
                    WithdrawSchema, 
                    this.withdraw.bind(this),
                    [ screenInfo.withdraw().join("") ]           
                )
                break;
            default:
                this.promptService.get( 
                    MenuDefaultSchema, 
                    this.menuDefault.bind(this), 
                    [ 
                        screenInfo.logout(this.session.username).join(""),
                        screenInfo.welcome().join("")
                    ] 
                )
                this.status = StatusAtm.END_SESSION;
                this.session = {}
                this.account.clear()
                break;
        }   
    }

    login(err: any, cb: any): any {
        if (err) { 
            console.log(err)
            return; 
        }
        const data = this.account.setUsername(cb.username).setPin(cb.pin);
        const myAccount = data.myAccount()
        if(data.isExist() && data.isCorrectPin()){
            this.promptService.get( 
                MenuTransaction, 
                this.menuTransaction.bind(this), 
                [ 
                    screenInfo.balance(myAccount.username,myAccount.balance).join(""),
                    screenInfo.myDebt(data.getMyDebt()).join(""),
                    screenInfo.myClaim(data.getMyClaim()).join("")
                ]
            )
            this.status = StatusAtm.IN_SESSION;
            this.session = {
                username: myAccount.username
            }
            return;
        }
        this.promptService.get(
            MenuDefaultSchema, 
            this.menuDefault.bind(this),
            [ screenInfo.wrongCredential().join("") ]
        )
    }

    register(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        const data = this.account
        .setUsername(cb.username)
        .setPin(cb.pin);
        if(data.isExist()){
            this.promptService.get(
                MenuDefaultSchema, 
                this.menuDefault.bind(this),
                [
                    screenInfo.usernameExist().join("")
                ]
            )
            return;
        }
        data.register()
        this.promptService.get(
            LoginSchema, 
            this.login.bind(this),
            [ 
                screenInfo.success().join(""),
                screenInfo.login().join("")
            ]
        )
    }

    deposit(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        let message = screenInfo.wrongPin().join("")
        let messagePayDebt = "";
        const data = this.account
        .setUsername(this.session.username)
        .setPin(cb.pin)
        .setBalance(cb.deposit);
        if(data.isCorrectPin()){
            const deposit = data.deposit()
            if(deposit){
                messagePayDebt = screenInfo.payMyDebt(deposit).join("")
            }
            message = screenInfo.success().join("")
        }
        this.promptService.get( 
            MenuTransaction, 
            this.menuTransaction.bind(this), 
            [ 
                message ,
                messagePayDebt,
                screenInfo.balance(
                    data.myAccount().username, 
                    data.myAccount().balance
                ).join(""),
                screenInfo.myDebt(data.getMyDebt()).join(""),
                screenInfo.myClaim(data.getMyClaim()).join("")
            ]
        )
    }

    transfer(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        let message = screenInfo.wrongPin().join("");
        const data = this.account
        .setUsername(this.session.username)
        .setPin(cb.pin)
        .setTransfer({
            username: cb.username,
            total: cb.total
        })
        if(!data.findTarget()){
            message = screenInfo.usernameNotExist().join("");
        }
        if(data.findTarget() && data.isCorrectPin()){ 
            const transfer = data.transfer();
            if(transfer === false){
                message = screenInfo.failedSelfTransfer().join("");
            }else{
                message = screenInfo.successTransfer(cb.username, transfer).join("");            
            }
        }
        this.promptService.get( 
            MenuTransaction, 
            this.menuTransaction.bind(this), 
            [ 
                message ,
                screenInfo.balance(
                    data.myAccount().username, 
                    data.myAccount().balance
                ).join(""),
                screenInfo.myDebt(data.getMyDebt()).join(""),
                screenInfo.myClaim(data.getMyClaim()).join("")
            ]
        )

    }

    withdraw(err: any, cb: any): void {
        if (err) { 
            console.log(err)
            return; 
        }
        let message = screenInfo.wrongPin().join("")
        const data = this.account
        .setUsername(this.session.username)
        .setPin(cb.pin)
        .setWithdraw(cb.withdraw);
        if(data.isCorrectPin()){
            const withdraw = data.withdraw();
            if(!withdraw){
                message = screenInfo.failedWithDraw().join("")
            }else{
                message = screenInfo.successWithDraw(withdraw).join("")
            }
        }
        this.promptService.get( 
            MenuTransaction, 
            this.menuTransaction.bind(this), 
            [ 
                message,
                screenInfo.balance(
                    data.myAccount().username, 
                    data.myAccount().balance
                ).join(""),
                screenInfo.myDebt(data.getMyDebt()).join(""),
                screenInfo.myClaim(data.getMyClaim()).join("")
            ]
        )
    }
    
}

export default AtmService;