import AccountService from "../accountService";

jest.useFakeTimers()

beforeAll(()=>{
})
afterEach(() => {
    jest.resetAllMocks();
});

describe('Account service', () => {
    describe('Check instance', () => {
        it('Should be same instance singleton', () => {
            const account1 = AccountService.createInstance();
            const account2 = AccountService.createInstance();
            expect(account1).toEqual(account2)
        });
    });
    describe('Check set value object', () => {
        it('should be correct value object builder pattern', () => {
            const account = AccountService.createInstance();
            const setUsername = account.setUsername("bob");
            const setPin = account.setPin(1234);
            const setBalance = account.setBalance(200);
            const setWithdraw = account.setWithdraw(100);

            expect(setUsername.temp.username).toEqual("bob")
            expect(setPin.temp.pin).toEqual(1234)
            expect(setBalance.temp.balance).toEqual(200)
            expect(setWithdraw.temp.withdraw).toEqual(100)
        })
    })

    describe('create account and validation', () => {

        let account = AccountService.createInstance();

        it('should add new account', () => {
            account
            .setUsername("bob")
            .setPin(1234)
            .register()
            expect(account.getLists().length).toEqual(1)
        })

        it('should be show count all lists account', () => {
            const lists = account.getLists()
            expect(lists.length).toEqual(1)
        })

        it('should be correct pin', () => {
            const isCorrectpin = account.isCorrectPin();
            expect(isCorrectpin).toEqual(true)
        })

        it('should be exist data', () => {
            const isExist = account.isExist();
            expect(isExist).toEqual(true)
        })

        it('should be correct my username', () => {
            const myAccount = account.myAccount();

            expect(myAccount.username).toEqual("bob")
        })

        it('should be correct my target transfer', () => {
            const registMytarget = account
            .setUsername("alice")
            .setPin(2222)
            .register()

            const setTarget = registMytarget
            .setTransfer({
                username: "alice",
                total: 10
            })
            const findTarget = setTarget.findTarget();

            expect(findTarget.username).toEqual("alice");
        })

        it('should be correct to find My Username debt', () => {
            
            const my = account
            .setUsername("bob")
            .setPin(1234)
            .myAccount();

            my.debt = [] 
            my.debt.push({...{
                    username: 'alice',
                    total: 10
                }
            })

            account.setDebt({
                username: 'alice',
                total: 10
            })
            const findMyDebtUsername = account.findMyDebtUsername();
            expect(findMyDebtUsername.username).toEqual("alice");
        })

        it('should be correct get My debt length', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .myAccount();

            const getMyDebt = account.getMyDebt();
            expect(getMyDebt.length).toEqual(1);
        })

        it('should be correct get My claim length', () => {
            
            account
            .setUsername("alice")
            .setPin(2222)
            .myAccount();

            const getMyClaim = account.getMyClaim();
            expect(getMyClaim.length).toEqual(1);
        })

        it('should be correct find My claim length', () => {
            
            account
            .setUsername("alice")
            .setPin(2222)
            .myAccount();

            account
            .setTransfer({
                username: "bob",
                total: 10
            })

            const findMyClaim = account.findMyClaim();
            expect(findMyClaim.username).toEqual("alice");
        })

        it('should be correct add My debt length', () => {
            
            account
            .setUsername("alice")
            .setPin(2222)
            .myAccount();

            account
            .setTransfer({
                username: "bob",
                total: 10
            })

            account.setDebt({
                username: 'example',
                total: 20
            })

            const addMyDebt = account.addMyDebt();

            expect(addMyDebt.length).toEqual(1);
            expect(addMyDebt[0].username).toEqual("example");
        })

        it('should be correct clear my debt with total is 0', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .myAccount();

            account.setDebt({
                username: 'example',
                total: 0
            })

            account.addMyDebt();
            account.clearMyPaidDebt();

            const getMyDebt = account.getMyDebt();

            expect(getMyDebt.length).toEqual(1);
        });

        it('should be correct clear my claim with total is 0', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .myAccount();

            account.setDebt({
                username: 'alice',
                total: 0
            })

            account.addMyDebt();
            account
            .setUsername("alice")
            .setPin(2222);

            account.clearMyPaidClaim();

            const getMyDebt = account.getMyClaim();

            expect(getMyDebt.length).toEqual(0);
        })


        it('should be correct withdraw all my balance', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .setWithdraw(200)
            .withdraw();

            account.clearMyPaidClaim();
            account.clearMyPaidDebt();

            const myAccount = account.myAccount()
            expect(myAccount.balance).toEqual(0);
        })

        it('should be correct deposit  my balance', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .setBalance(200)
            .deposit()

            const myAccount = account.myAccount()

            expect(myAccount.balance).toEqual(200);
        })

        it('should be correct transfer 100 to alice', () => {
            
            account
            .setUsername("bob")
            .setPin(1234)
            .setTransfer({
                username: "alice",
                total: 200
            })
            .transfer()

            const myAccount = account.myAccount()
            expect(myAccount.balance).toEqual(0);
        })
        

    })
})
