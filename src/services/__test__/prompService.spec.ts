import PromptService from "../promptService";

jest.useFakeTimers()

beforeAll(()=>{
})
afterEach(() => {
    jest.resetAllMocks();
});
describe('Prompt service', () => {
    describe('Check instance', () => {
        it('Should be same instance singleton', () => {
            const prompt1 = PromptService.createInstance();
            const prompt2 = PromptService.createInstance();
            expect(prompt1).toEqual(prompt2)
        });
    });
})