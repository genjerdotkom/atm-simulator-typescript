import AtmService from "../atmService"

jest.useFakeTimers()

beforeAll(()=>{
})
afterEach(() => {
    jest.resetAllMocks();
});
describe('Atm service', () => {
    describe('Check instance', () => {
        it('Should be same instance singleton', () => {
            const atm1 = AtmService.createInstance();
            const atm2 = AtmService.createInstance();
            expect(atm1).toEqual(atm2)
        });
    });
})