import prompt from "prompt";
import screenInfo from "../helpers/screenInfo";
class PromptService{
    private static instance:PromptService;

    public static createInstance(): PromptService{
        if(!PromptService.instance){
            PromptService.instance = new PromptService();
        }
        return PromptService.instance;
    }

    public get (Schema: any, callback: any, status?: any){ 
        screenInfo.clear()
        if(typeof status !== 'undefined'){
            status.map((item: string) => {
                screenInfo.custom(item);
            })
        }
        return prompt.get( Schema, callback);
    }

}

export default PromptService