import { 
    TempType,
    listsType
} from "../entities/account";

class AccountService {
    private temp: TempType;
    private lists?: listsType;
    private static instance:AccountService;

    constructor(){
        this.temp = {
            username: '',
            transfer: {
                username: '',
                total: 0
            },
            withdraw:0,
            pin: 0,
            balance: 0,
            debt:{
                username: '',
                total: 0
            }
        }
        this.lists = [];
    }

    public static createInstance(): AccountService{
        if(!AccountService.instance){
            AccountService.instance = new AccountService();
        }
        return AccountService.instance;
    }

    public clear(){
        this.temp.username = '';
        this.temp.pin = 0;
        this.temp.balance = 0;
        this.temp.debt = {
            username: '',
            total: 0
        };
        this.temp.transfer = {
            username: '',
            total: 0
        };
        return this
    }

    public setUsername(value: string): any{
        this.temp.username = value;
        return this
    }

    public setTransfer(value: any): any{
        this.temp.transfer = value
        return this
    }

    public setDebt(value: any): any{
        this.temp.debt = value
        return this
    }

    public setPin(value: number): any{
        this.temp.pin = value;
        return this
    }

    public setBalance(value: number): any{
        this.temp.balance = value;
        return this
    }

    public setWithdraw(value: number): any{
        this.temp.withdraw = value;
        return this
    }

    public getLists(): any{
        return this.lists
    }

    public isCorrectPin(): boolean{
        const pin = this.lists.find((x: any) => x.username === this.temp.username).pin;
        if(pin === this.temp.pin){
            return true
        }
        return false
    }

    public isExist(): boolean{
        for (let x in this.lists) {
            if (this.lists[x].username === this.temp.username) {
                return true;
            }
        }
        return false;
    }

    public myAccount(){
        return this.lists.find((x: any) => x.username === this.temp.username);
    }

    public register(){
        this.lists.push({...this.temp});
        return this
    }

    public findTarget(){
        return this.lists.find((x: any) => x.username === this.temp.transfer.username);
    }

    public findMyDebtUsername(){
        const my = this.myAccount();
        if(typeof my.debt !== 'undefined' && my.debt.length > 0){
            return my.debt.find((x: any) => x.username === this.temp.debt.username);
        }
        return false;
    }

    public getMyDebt(){
        const my = this.myAccount();
        if(typeof my.debt !== 'undefined' && my.debt.length > 0){
            return my.debt
        }
        return false;
    }

    public getMyClaim(){
        let data:any = []
        const myUsername = this.temp.username
        const res = this.lists.filter(function f(o: any) {
            return !o.username.includes(myUsername) &&
            Array.isArray(o.debt) && (o.debt.filter((x: any) => x.username === myUsername)).length
        })
        if(res.length > 0){
            res.map((item: any) =>{
                let total = item.debt.find((x: any) => x.username === myUsername).total
                data.push({
                    username: item.username,
                    total: total
                })
            })
        }
        return data
    } 

    public findMyClaim(){
        const myUsername = this.temp.username;
        const target = this.findTarget();
        if(typeof target.debt !== 'undefined' && target.debt.length > 0){
            return target.debt.find((x: any) => x.username === myUsername)
        }
        return false;
    }

    public addMyDebt(){
        const my = this.myAccount();
        const findMyDebt = this.findMyDebtUsername();
        if(findMyDebt){
            findMyDebt.total = findMyDebt.total + +this.temp.debt.total
        }else{
            if(!Array.isArray(my.debt)){
                my.debt = []            
            }
            my.debt.push({...{
                    username: this.temp.debt.username, 
                    total: this.temp.debt.total
                }
            });
        }
        return my.debt
    }

    public clearMyPaidDebt() {
        const myDebt = this.getMyDebt();
        for (let i = myDebt.length - 1; i >= 0; i--) {
            if (myDebt[i].total === 0) { 
                myDebt.splice(i, 1);
            }
        }
        return;
    }

    public clearMyPaidClaim() {
        const myUsername = this.temp.username
        const target = this.findTarget();
        if(typeof target.debt !== 'undefined' && target.debt.length > 0){
            const removeIndex = target.debt.findIndex( (item: any) => 
            item.username === myUsername && item.total === 0 );
            target.debt.splice( removeIndex, 1 );
        }
        return;
    }

    public withdraw(){
        const my = this.myAccount();
        if(my.balance >= this.temp.withdraw){
            my.balance = my.balance - +this.temp.withdraw
            return this.temp.withdraw
        }
        return false
    }

    public deposit(){ 
        const my = this.myAccount();
        const myDebt = this.getMyDebt();
        let myBalance = +my.balance + +this.temp.balance;
        let sisa = myBalance;
        let tranferedList: any = []
        if(myDebt){
            let totalTransfer = 0
            let itemTotal = 0
            myDebt.map((item: any) => {
                this.temp.transfer.username = item.username
                let target = this.findTarget();
                sisa = +myBalance - +item.total
                totalTransfer = myBalance;
                itemTotal = +item.total - +myBalance;
                if(item.total <= +myBalance ){
                    totalTransfer = item.total;
                    itemTotal = 0 
                }
                tranferedList.push({
                    username: item.username,
                    total: totalTransfer
                })
                item.total = itemTotal
                target.balance = target.balance + +totalTransfer;
                myBalance = (sisa <= 0) ? 0 : sisa
            })
            this.clearMyPaidDebt()
        }
        my.balance = myBalance
        return tranferedList
    } 
 
    public transfer(){
        const my = this.myAccount();
        const target = this.findTarget();
        const username = this.temp.transfer.username
        let findMyClaim = this.findMyClaim();
        let total = this.temp.transfer.total;
        if(this.temp.username === username){
            return false
        }
        if(findMyClaim) {
            if(total <= findMyClaim.total){
                findMyClaim.total = findMyClaim.total - +total
                total = 0;
            }else{
                total = +total - findMyClaim.total;
                findMyClaim.total =  0 
            }
            if(findMyClaim.total == 0){
                this.clearMyPaidClaim();
            }
        }
        if(my.balance < total){ 
            this.setDebt({
                username,
                total: +total - +my.balance
            });
            this.addMyDebt()
            target.balance = +target.balance + +my.balance;
            my.balance = 0;
            return my.balance;
        }
        my.balance = +my.balance - +total;
        target.balance = +target.balance + +total;
        return total;
    }

}

export default AccountService

