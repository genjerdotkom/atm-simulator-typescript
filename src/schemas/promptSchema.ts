import chalk from "chalk";

const MenuDefaultSchema: any = {
    properties: {
        menu: {
            description:`
                ${chalk.yellow("\n[1] Login \n[2] Create New Account")}
                ${chalk.blueBright("\nchoose")}`,
            pattern: /^[1-2]+$/,
            message: `${chalk.red("Choose 1 or 2")}`,
            required: true, 
            hidden: true
        }
    }
}

const MenuTransaction: any = {
    properties: {
        menu: {
            description:`
                ${chalk.yellow("\n[1] Deposit \n[2] Transfer \n[3] Withdraw \n[0] Logout")}
                ${chalk.blueBright("\nchoose")}`,
            pattern: /^[0-3]+$/,
            message: `${chalk.red("Choose 1, 2, 3 or 0")}`,
            required: true, 
            hidden: true
        }
    }
}

const LoginSchema: any = {
    properties: {
        username: {
            description:`${chalk.yellow("username")}`,
            pattern: /^\S*$/,
            message: `${chalk.red("Username consisting only of non-whitespaces")}`,
            required: true, 
            hidden: true
        },
        pin: {
            description: `${chalk.yellow("pin")}`,
            pattern: /^[0-9][0-9][0-9][0-9]$/,
            message: `${chalk.red("pin must be 4 digit number..")}`,
            required: true,
            hidden: true
        }
    }
}  

const RegisterSchema: any = {
    properties:{
        username: {
            description:`${chalk.yellow("username")}`,
            pattern: /^\S*$/,
            message: `${chalk.red("Username consisting only of non-whitespaces")}`,
            required: true, 
            hidden: true
        },
        pin: {
            description: `${chalk.yellow("pin <4 digit number>")}`,
            pattern: /^[0-9][0-9][0-9][0-9]$/,
            message: `${chalk.red("pin must be 4 digit number..")}`,
            required: true,
            hidden: true
        }
    }
}

const DepositSchema: any = {
    properties:{
        deposit: {
            description:`${chalk.yellow("amount")}`,
            pattern: /^[1-9]\d*(\.\d+)?$/,
            message: `${chalk.red("Please type valid number!")}`,
            required: true, 
            hidden: true 
        },
        pin: {
            description: `${chalk.yellow("pin <4 digit number>")}`,
            pattern: /^[0-9][0-9][0-9][0-9]$/,
            message: `${chalk.red("pin must be 4 digit number..")}`,
            required: true,
            hidden: true
        }
    }
}

const WithdrawSchema: any = {
    properties:{
        withdraw: {
            description:`${chalk.yellow("amount")}`,
            pattern: /^[1-9]\d*(\.\d+)?$/,
            message: `${chalk.red("Please type valid number!")}`,
            required: true, 
            hidden: true 
        },
        pin: {
            description: `${chalk.yellow("pin <4 digit number>")}`,
            pattern: /^[0-9][0-9][0-9][0-9]$/,
            message: `${chalk.red("pin must be 4 digit number..")}`,
            required: true,
            hidden: true
        }
    }
}

const TransferSchema: any = {
    properties:{
        username: {
            description:`${chalk.yellow("transfer to <username>")}`,
            pattern: /^\S*$/,
            message: `${chalk.red("Username consisting only of non-whitespaces")}`,
            required: true, 
            hidden: true
        },
        total: {
            description:`${chalk.yellow("total")}`,
            pattern: /^[1-9]\d*(\.\d+)?$/,
            message: `${chalk.red("Please type valid number!")}`,
            required: true, 
            hidden: true
        },
        pin: {
            description: `${chalk.yellow("pin <4 digit number>")}`,
            pattern: /^[0-9][0-9][0-9][0-9]$/,
            message: `${chalk.red("pin must be 4 digit number..")}`,
            required: true,
            hidden: true
        }
    }
}

export {
    LoginSchema,
    RegisterSchema,
    MenuDefaultSchema,
    MenuTransaction,
    DepositSchema,
    TransferSchema,
    WithdrawSchema
};