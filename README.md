# ATM Simulator - typescript
## Getting started
### Requirements

- NodeJS v12.12.0 Or higher

### Installation

```console
npm install
```

Build Code

```console
npm run tsc
```

### Run Application

Run Application

```console
npm run dev
```

Exit Application

```console
ctrl + c
```

### Unit Test Jess

Test and watch

```console
npm run test:watch
```

Test and get coverage

```console
npm run test
```

### Screenshoots

![ss1](screenshoots/ss1.png)
![ss2](screenshoots/ss2.png)
![ss3](screenshoots/ss3.png)
![ss4](screenshoots/ss4.png)
![ss5](screenshoots/ss5.png)

### Thank You 

GinanjarDP
[Linkeddin](https://id.linkedin.com/in/ginanjar-putranto-0416a913b)
