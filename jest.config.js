/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: [
    '<rootDir>/**/__tests__/**/?(*.)(spec|test).ts',
    '<rootDir>/**/?(*.)(spec|test).ts'
  ],
  collectCoverageFrom: [
    "src/services/*.ts*"
  ]
};